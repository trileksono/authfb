package com.example.fb.entity;

import org.springframework.data.annotation.Id;

/**
 * Created by tri on 12/7/16.
 */
public class Sentiment {

  @Id
  private String id;
  private String idUser;
  private String idStatus;
  private float probabilitas;
  private int sentimen;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getIdUser() {
    return idUser;
  }

  public void setIdUser(String idUser) {
    this.idUser = idUser;
  }

  public String getIdStatus() {
    return idStatus;
  }

  public void setIdStatus(String idStatus) {
    this.idStatus = idStatus;
  }

  public float getProbabilitas() {
    return probabilitas;
  }

  public void setProbabilitas(float probabilitas) {
    this.probabilitas = probabilitas;
  }

  public int getSentimen() {
    return sentimen;
  }

  public void setSentimen(int sentimen) {
    this.sentimen = sentimen;
  }
}
