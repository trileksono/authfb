package com.example.fb.repository;

import com.example.fb.entity.Sentiment;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by tri on 12/8/16.
 */
public interface SentimenRepository extends MongoRepository<Sentiment,String>{

  List<Sentiment> findByidUser(String idUser);

}
