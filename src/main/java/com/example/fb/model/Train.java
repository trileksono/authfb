package com.example.fb.model;

/**
 * Created by tri on 12/9/16.
 */
public class Train {
  private String status;
  private String sentimen;

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getSentimen() {
    return sentimen;
  }

  public void setSentimen(String sentimen) {
    this.sentimen = sentimen;
  }
}
