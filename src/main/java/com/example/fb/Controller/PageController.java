/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.fb.Controller;

import com.example.fb.entity.*;
import com.example.fb.naive.BayesClassifier;
import com.example.fb.naive.Classification;
import com.example.fb.naive.Classifier;
import com.example.fb.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author tri
 */
@Controller
public class PageController {

  @Autowired
  StatusRepository rStatus;

  @Autowired
  SingkatanRepository rSingkatan;

  @Autowired
  StopWordRepository rStopWord;

  @Autowired
  KamusRepository rKamus;

  @Autowired
  TrainingRepository rTraining;

  @Autowired
  SentimenRepository rSentiment;

  @Autowired
  MemberRepository rMember;

  private Matcher m;
  private Pattern p;
  private StringTokenizer str;

  @RequestMapping(value = "page/{name}", method = RequestMethod.GET)
  public void redirect(@PathVariable("name") String name, Model m, HttpServletRequest request) {
  }

  @RequestMapping(value = "page/group_post/{page}", method = RequestMethod.GET)
  public ModelAndView getGroupPost(
        ModelAndView m,
        HttpServletRequest request,
        @PathVariable("page") int page) {
    m.addObject("list", rStatus.findStatusOnly(new PageRequest(page, 50)));
    m.setViewName("page/GroupPost");
    return m;
  }

  @RequestMapping(value = "page/post/{id}", method = RequestMethod.GET)
  public ModelAndView getPost(
        ModelAndView m,
        HttpServletRequest request,
        @PathVariable("id") String id) {
    Status sts = rStatus.findOne(id);
    m.addObject("post", sts);
    m.addObject("comment", rStatus.findStatusByIdParent(sts.getId()));
    m.setViewName("page/PostComment");
    return m;
  }

  @RequestMapping(value = "page/ListSummary", method = RequestMethod.GET)
  public ModelAndView getSummary(
        ModelAndView m,
        HttpServletRequest request,
        @RequestParam("id") String id) {
    List lst = new ArrayList();

    List<Sentiment> sentiment = rSentiment.findByidUser(id);
    for (Sentiment snt : sentiment) {
      Map map = new HashMap();
      map.put("sentiment", snt);
      map.put("status", rStatus.findOne(snt.getIdStatus()));
      lst.add(map);
    }
    m.addObject("list", lst);
    m.setViewName("page/ListSummary");
    return m;
  }

  @RequestMapping(value = "page/testing", method = RequestMethod.GET)
  public ModelAndView doTesting(
        ModelAndView m,
        HttpServletRequest request,
        @RequestParam(value = "id", required = true) String id,
        @RequestParam(value = "jumlah") int jumlah) {

    Classifier<String, String> bayes = new BayesClassifier<String, String>();
    List<Member> lmember = new ArrayList<>();
    try {
      rSentiment.deleteAll();
      bayes.learn("-1", getNegatif());
      bayes.learn("1", getPositif());
      String[] data = id.split(",");
      if (data.length > 0) {
        for (int x = 0; x < data.length; x++) {
          String idMember = data[x];
          lmember.add(rMember.findOne(idMember));
          List<Status> list = rStatus.findStatusByidUser(idMember, new PageRequest(0, jumlah));

          for (int y = 0; y < list.size(); y++) {
            Classification cl = doCalculate(list.get(y).getMessage(), bayes);
            Sentiment stm = new Sentiment();
            stm.setIdStatus(list.get(y).getId());
            stm.setIdUser(idMember);
            if (cl == null) {
              stm.setProbabilitas(0);
              stm.setSentimen(0);
            } else {
              stm.setProbabilitas(cl.getProbability());
              stm.setSentimen(Integer.parseInt(cl.getCategory().toString()));
            }
            rSentiment.save(stm);
          }
        }
        bayes.setMemoryCapacity(500);
      }

    } catch (Exception ex) {
      ex.printStackTrace();
    }
    List lst = new ArrayList();
    for (Member mb : lmember) {
      int x = 0;
      List<Sentiment> sentimentList = rSentiment.findByidUser(mb.getId());
      Map listItem = new HashMap();
      listItem.put("member", mb);
      listItem.put("sentimen", sentimentList);
      for (Sentiment st : sentimentList) {
        x += st.getSentimen();
      }
      listItem.put("total", x);
      lst.add(listItem);
    }
    m.addObject("list", lst);
    m.setViewName("page/Summary");
    return m;
  }

  private Classification doCalculate(String status, Classifier<String, String> bayes) {
    String bersih = bersihkan(status);
    if (bersih == null) return null;
    return bayes.classify(Arrays.asList(bersih.split("\\s")));
  }

  private List<String> getPositif() {
    List<String> listPositif = new ArrayList<>();
    for (Training tr : rTraining.findTraining("1")) {
      listPositif.add(tr.getKata());
    }
    return listPositif;
  }

  private List<String> getNegatif() {
    List<String> listNegatif = new ArrayList<>();
    for (Training tr : rTraining.findTraining("-1")) {
      listNegatif.add(tr.getKata());
    }
    return listNegatif;
  }

  public String bersihkan(String tweet) {
    // ## Pattern untuk mencari kata URL
    if (tweet == null) {
      return null;
    }
    String s = "";
    int adjectiva = 0;
    try {
      tweet = tweet.toLowerCase();
      String urlPattern = "((https?|http):[\\w\\d:#@%/;$()~_?+-=\\\\.&]*)";
      p = Pattern.compile(urlPattern, Pattern.CASE_INSENSITIVE);
      m = p.matcher(tweet);

      int i = 0;
      while (m.find()) {
        tweet = tweet.replaceAll(m.group(i), "");
        i++;
      }
      // end //

		/*
    1. hilangkan {}()[]
        2. Hilangkan kata @ #
        3. Hilangkan enter(line)
        5. hilangkan petik
        6. hilangkan selain huruf a-z
        7. hilangkan double spasi
		*/
      tweet = tweet.replaceAll("\\(.+?\\)|\\[.+?\\]|\\{.+?\\}", "");
      tweet = tweet.replaceAll("@\\w+|#\\w", "");
      tweet = tweet.replaceAll("\n", " ");
      tweet = tweet.replaceAll("[']", "");
      tweet = tweet.replaceAll("[^a-z ]+", " ");
      tweet = tweet.replaceAll(" +", " ").trim();
      // pecah Kalimat perkata ~> lakukan CarsiSingkatan, Stemming ~> Satukan kata menjadi kalimat
      if (tweet.length() > 1) {
        str = new StringTokenizer(tweet);
        while (str.hasMoreElements()) {
          String k = str.nextToken();
          if (findAdjectiv(k) != null) adjectiva++;
          if (k.trim().length() > 1) {
            s += " " + stopWord(zief(cariSingkatan(k))); //Cari singkatan, Stemming
          } else {
            s += "";
          }
        }
        if (s.length() > 1) {
          s = s.replaceAll("\\s+", " ");
          s = s.trim();
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    // ## End ##
    if (adjectiva == 0) {
      return null;
    }
    return s;
  }

  private String cariSingkatan(String kata) {
    Singkatan skt = rSingkatan.findSingkatanByKataSingkatan(kata);
    if (skt == null) {
      return kata;
    } else {
      return skt.getKataAsli();
    }
  }

  private String stopWord(String kata) {
    StopWord st = rStopWord.findStopWordByKataStop(kata);
    if (st == null) {
      return kata;
    } else {
      return st.getKataStop();
    }
  }

  public Kamus findAdjectiv(String kata) {
    return rKamus.findAdjektiva(kata);
  }

  public boolean cariKamus(String s) {
    if (rKamus.findKamusByKata(s) == null) {
      return false;
    } else {
      return true;
    }
  }

  public String zief(String kataAsal) {
    String kata = kataAsal;
    if (cariKamus(kata)) {
      //System.out.println("s");
      return kata;
    }
    if (kata.matches(".*([lk]ah$|[km]u$|nya$|pun$).*")) {
      //System.out.println("1");
      kata = hapus_inflection(kata);
    }
    if (kata.matches(".*(i$|an$).*")) {
      //System.out.println("2");
      kata = hapusDerivationSuffixes(kata);
    }
    if (kata.matches(".*(^di|^[ksmbpt]e).*")) {
      //System.out.println("3");
      kata = hapusDerivateionPrefix(kata);
    }
    return kata;
  }

  private String hapus_inflection(String s) {
    String kata = s.replaceAll("([lk]ah|[km]u|nya|pun)$", "");
    if (cariKamus(kata)) {
      return kata;
    }
    if (s.matches("([lk]ah|pun)$")) {
      if (kata.matches("([km]u|nya)$")) {
        String katabaru = kata.replaceAll("([km]u|nya)$", "");
        if (cariKamus(katabaru)) {
          return katabaru;
        }
      }
    }
//        if (s.endsWith("kah") || s.endsWith("lah") || s.endsWith("pun")) {
//            kata = s.substring(0, s.length() - 3);
//            cariKamus(kata);
//        }
//        if (s.endsWith("ku") || s.endsWith("mu")) {
//            kata = s.substring(0, s.length() - 2);
//            cariKamus(kata);
//        }
//        if (s.endsWith("nya")) {
//            kata = s.substring(0, s.length() - 3);
//            cariKamus(kata);
//        }
    return kata;
  }

  private boolean cekPrefixesGabungSuffixes(String s) {
    if (s.startsWith("be") && s.endsWith("i")) {
      return true;
    }
    if (s.startsWith("di") && s.endsWith("an")) {
      return true;
    }
    if (s.startsWith("ke") && s.endsWith("i")) {
      return true;
    }
    if (s.startsWith("ke") && s.endsWith("kan")) {
      return true;
    }
    if (s.startsWith("se") && s.endsWith("i")) {
      return true;
    }
    if (s.startsWith("se") && s.endsWith("kan")) {
      return true;
    }
    if (s.startsWith("me") && s.endsWith("an")) {
      return true;
    }
    if (s.startsWith("te") && s.endsWith("an")) {
      return true;
    }
    return false;
  }

  private String hapusDerivationSuffixes(String s) {
    String kata = s;
//        if (s.matches(".*(i|an)$")) {
//            kata = s.replaceAll("(i|an)$", "");
//            if (cariKamus(kata)) {
//                return kata;
//            }
//        } else {
//            if (s.matches(".*(k)$")) {
//                kata = s.replaceAll("k$", "");
//                if (cariKamus(kata)) {
//                    return kata;
//                }
//            }
//        }
    if (s.endsWith("kan")) {
      kata = s.replaceAll("kan$", "");
      cariKamus(kata);
      return kata;
    } else if (s.endsWith("i")) {
      kata = s.replaceAll("i$", "");
      cariKamus(kata);
      return kata;
    } else if (s.endsWith("an")) {
      kata = s.replaceAll("an$", "");
      cariKamus(kata);
      return kata;
    }

    return kata;
  }

  private String hapusDerivateionPrefix(String s) {
    String kata = s;
    String katabaru = s;
    String ketemu = "";
    if (cekPrefixesGabungSuffixes(s)) {
      return kata;
    } else {
      for (int i = 0; i <= 2; i++) {
        if (s.matches(".*(^di|^[ksmbpt]e).*")) {//s.startsWith("se") || s.startsWith("me") || s.startsWith("ke") || s.startsWith("di") || s.startsWith("be") || s.startsWith("pe") || s.startsWith("te")) {
          if (s.matches("(^di|^[ks]e).*")) {//s.startsWith("ke") || s.startsWith("di") || s.startsWith("se")) {
            kata = s.substring(2, s.length());
            cariKamus(kata);
          } else {
            kata = cekPrefiksBermorfologi(kata);
            if (cariKamus(kata)) {
              return kata;
            }
          }
        }
      }
    }

    return kata;
  }

  private String cekPrefiksBermorfologi(String s) {
    String kataStemAsal = s;
    String kata;
    String kataBaru;
    if (kataStemAsal.matches("(^[mbpt]e).*")) {//s.startsWith("me") || s.startsWith("be") || s.startsWith("pe") || s.startsWith("te")) {
      if (s.startsWith("be")) {
        // ========== rule 1 ===========
        if (s.matches(".*((^ber)[aiueo]).*")) {
          kata = s.substring(3, s.length());
          if (cariKamus(kata)) {
            return kata;
          }
          kataBaru = hapusDerivationSuffixes(kata);
          if (cariKamus(kataBaru)) {
            return kataBaru;
          }
          kata = "r" + s.substring(3, s.length());
          if (cariKamus(kata)) {
            return kata;
          }
          kataBaru = hapusDerivationSuffixes(kata);
          return kataBaru;
        }
        //============ rule 2 ==============
        if (s.matches(".*((^ber)[^aiueor]).*")) { //ber a kata -er
          kata = s.substring(3, s.length());
          if (cariKamus(kata)) {
            return kata;
          }
          kataBaru = hapusDerivationSuffixes(kata);
          return kataBaru;
        }
        //=========== rule 3 ==============
        if (s.matches(".*((^ber)[^aiueor]).*(er)[aiueo]")) {
          kata = s.substring(3, s.length());
          if (cariKamus(kata)) {
            return kata;
          }
          kataBaru = hapusDerivationSuffixes(kata);
          return kataBaru;
        }
                /* ------------rule 4 ---------------*/
        if (s.startsWith("bel")) {
          kata = s.substring(3, s.length());
          if (cariKamus(kata)) {
            return kata;
          }
          kataBaru = hapusDerivationSuffixes(kata);
          return kataBaru;
        }
        //============ rule 5 =======
        if (s.matches(".*((^ber)[^aiueor]).*(er)[^aiueo]")) {
          kata = s.substring(2, s.length());
          if (cariKamus(kata)) {
            return kata;
          }
          kataBaru = hapusDerivationSuffixes(kata);
          return kataBaru;
        }
      }
      // ====== awalan te- =============
      if (s.startsWith("te")) {
        //======= rule 6 ========
        if (s.matches(".*((^ter)[aiueo]).*")) {
          kata = "r" + s.substring(3, s.length());
          if (cariKamus(kata)) {
            return kata;
          }
          kataBaru = hapusDerivationSuffixes(kata);
          if (cariKamus(kataBaru)) {
            return kataBaru;
          }
          kata = s.replaceAll("/*(^ter)", "");
          if (cariKamus(kata)) {
            return kata;
          }
          kataBaru = hapusDerivationSuffixes(kata);
          return kataBaru;
        }

        // ========= rule 7 ===========
        if (s.matches(".*((^ter)[^aiueor]er[aiueo]).*")) {
          kata = s.replaceAll("(^ter)", "r");
          if (cariKamus(kata)) {
            return kata;
          }
          kataBaru = hapusDerivationSuffixes(kata);
          return kataBaru;
        }

        //============ rule 8 ===========
        if (s.matches(".*((^ter)[^aiueor]).*")) {
          kata = s.replaceAll("(^ter)", "");
          if (cariKamus(kata)) {
            return kata;
          }
          kataBaru = hapusDerivationSuffixes(kata);
          return kataBaru;
        }
        //============ rule 9 ===========
        if (s.matches(".*((^te)[^aiueor]er[^aiueo]).*")) {
          kata = s.replaceAll("(^te)", "");
          if (cariKamus(kata)) {
            return kata;
          }
          kataBaru = hapusDerivationSuffixes(kata);
          return kataBaru;
        }
      }
      //========= awalan me- ==============
      if (s.matches("^(me).*")) {
        //============ rule 10 ===========
        if (s.matches("((^me)[lrwy][aiueo]).*")) {
          kata = s.replaceAll("(^me)", "");
          if (cariKamus(kata)) {
            return kata;
          }
          kataBaru = hapusDerivationSuffixes(kata);
          return kataBaru;
        }
        //============ rule 11 ===========
        if (s.matches(".*((^mem)[bfvp]).*")) {
          kata = s.replaceAll("(^mem)", "");
          if (cariKamus(kata)) {
            return kata;
          }
          kataBaru = hapusDerivationSuffixes(kata);
          return kataBaru;
        }
        //============ rule 12 ===========
        if (s.matches(".*((^mempe)[rl]).*")) {
          kata = s.replaceAll("(^mempe)", "pe");
          if (cariKamus(kata)) {
            return kata;
          }
          kataBaru = hapusDerivationSuffixes(kata);
          return kataBaru;
        }
        //============ rule 13 ===========
        if (s.matches(".*((^mem)(r[aiueo]|[aiueo])).*")) {
          kata = s.replaceAll("(^mem)", "m");
          if (cariKamus(kata)) {
            return kata;
          }
          kataBaru = hapusDerivationSuffixes(kata);
          if (cariKamus(kataBaru)) {
            return kataBaru;
          }
          kata = s.replaceAll("(^mem)", "p");
          if (cariKamus(kata)) {
            return kata;
          }
          kataBaru = hapusDerivationSuffixes(kata);
          return kataBaru;
        }
        //============ rule 14 ===========
        if (s.matches(".*((^men)[cdjzst]).*")) {
          kata = s.replaceAll("(^men)", "");
          if (cariKamus(kata)) {
            return kata;
          }
          kataBaru = hapusDerivationSuffixes(kata);
          return kataBaru;
        }
        //============ rule 15 ===========
        if (s.matches(".*((^men)[aiueo]).*")) {
          kata = s.replaceAll("(^men)", "n");
          if (cariKamus(kata)) {
            return kata;
          }
          kataBaru = hapusDerivationSuffixes(kata);
          if (cariKamus(kataBaru)) {
            return kataBaru;
          }
          kata = s.replaceAll("(^men)", "t");
          if (cariKamus(kata)) {
            return kata;
          }
          kataBaru = hapusDerivationSuffixes(kata);
          return kataBaru;
        }
        //============ rule 16 ===========
        if (s.matches(".*((^meng)[ghqk]).*")) {
          kata = s.replaceAll("(^meng)", "");
          if (cariKamus(kata)) {
            return kata;
          }
          kataBaru = hapusDerivationSuffixes(kata);
          return kataBaru;
        }
        //============ rule 17 ===========
        if (s.matches("(^meng)[aiueo].*")) {
          kata = s.replaceAll("(^meng)", "");
          if (cariKamus(kata)) {
            return kata;
          }
          kataBaru = hapusDerivationSuffixes(kata);
          if (cariKamus(kataBaru)) {
            return kataBaru;
          }
          kata = s.replaceAll("(^meng)", "k");
          if (cariKamus(kata)) {
            return kata;
          }
          kataBaru = hapusDerivationSuffixes(kata);
          return kataBaru;
        }
        //============ rule 18 ===========
        if (s.matches(".*(^meny)[aiueo].*")) {
          kata = s.replaceAll("(^meny)", "s");
          if (cariKamus(kata)) {
            return kata;
          }
          kataBaru = hapusDerivationSuffixes(kata);
          return kataBaru;
        }
        //============ rule 19 ===========
        if (s.matches(".*((^memp)[aiueo]).*")) {
          kata = s.replaceAll("(^memp)", "p");
          if (cariKamus(kata)) {
            return kata;
          }
          kataBaru = hapusDerivationSuffixes(kata);
          return kataBaru;
        }
      }
      //============= awalan pe ===========
      if (s.matches("(^pe).*")) {
        //============ rule 20 ===========
        if (s.matches(".*((^pe)[wy][aiueo]).*")) {
          kata = s.replaceAll("(^pe)", "");
          if (cariKamus(kata)) {
            return kata;
          }
          kataBaru = hapusDerivationSuffixes(kata);
          return kataBaru;
        }
        //============ rule 21 ===========
        if (s.matches(".*((^per)[aiueo]).*")) {
          kata = s.replaceAll("(^per)", "");
          if (cariKamus(kata)) {
            return kata;
          }
          kataBaru = hapusDerivationSuffixes(kata);
          if (cariKamus(kataBaru)) {
            return kataBaru;
          }
          kata = s.replaceAll("(^per)", "r");
          if (cariKamus(kata)) {
            return kata;
          }
          kataBaru = hapusDerivationSuffixes(kata);
          return kataBaru;
        }
        //============ rule 22 ===========
        if (s.matches("(^per)[^aiueor][a-z]*(?!er).*")) {
          kata = s.replaceAll("(^per)", "");
          if (cariKamus(kata)) {
            return kata;
          }
          kataBaru = hapusDerivationSuffixes(kata);
          return kataBaru;
        }
        //============ rule 23 ===========
        if (s.matches("(^per)[^aiueor][a-z]*(er[aiueo]).*")) {
          kata = s.replaceAll("(^per)", "");
          if (cariKamus(kata)) {
            return kata;
          }
          kataBaru = hapusDerivationSuffixes(kata);
          return kataBaru;
        }
        //============ rule 24 ===========
        if (s.matches("(^pem)[bfv].*")) {
          kata = s.replaceAll("(^pem)", "");
          if (cariKamus(kata)) {
            return kata;
          }
          kataBaru = hapusDerivationSuffixes(kata);
          return kataBaru;
        }
        //============ rule 25 ===========
        if (s.matches(".*((^pem)(r[aiueo]|[aiueo])).*")) {
          kata = s.replaceAll("(^pem)", "m");
          if (cariKamus(kata)) {
            return kata;
          }
          kataBaru = hapusDerivationSuffixes(kata);
          if (cariKamus(kataBaru)) {
            return kataBaru;
          }
          kata = s.replaceAll("(^pem)", "p");
          if (cariKamus(kata)) {
            return kata;
          }
          kataBaru = hapusDerivationSuffixes(kata);
          return kataBaru;
        }
        //============ rule 26 ===========
        if (s.matches("(^pen)[cdjz].*")) {
          kata = s.replaceAll("(^pen)", "");
          if (cariKamus(kata)) {
            return kata;
          }
          kataBaru = hapusDerivationSuffixes(kata);
          return kataBaru;
        }
        //============ rule 27 ===========
        if (s.matches(".*(^pen)[aiueo].*")) {
          kata = s.replaceAll("(^pen)", "n");
          if (cariKamus(kata)) {
            return kata;
          }
          kataBaru = hapusDerivationSuffixes(kata);
          if (cariKamus(kataBaru)) {
            return kataBaru;
          }
          kata = s.replaceAll("(^pen)", "t");
          if (cariKamus(kata)) {
            return kata;
          }
          kataBaru = hapusDerivationSuffixes(kata);
          return kataBaru;
        }
        //============ rule 28 ===========
        if (s.matches("(^peng)[ghqk].*")) {
          kata = s.replaceAll("(^peng)", "");
          if (cariKamus(kata)) {
            return kata;
          }
          kataBaru = hapusDerivationSuffixes(kata);
          return kataBaru;
        }
        //============ rule 29 ===========
        if (s.matches(".*(^peng)[aiueo].*")) {
          kata = s.replaceAll("(^peng)", "");
          if (cariKamus(kata)) {
            return kata;
          }
          kataBaru = hapusDerivationSuffixes(kata);
          if (cariKamus(kataBaru)) {
            return kataBaru;
          }
          kata = s.replaceAll("(^peng)", "k");
          if (cariKamus(kata)) {
            return kata;
          }
          kataBaru = hapusDerivationSuffixes(kata);
          return kataBaru;
        }
        //============ rule 30 ===========
        if (s.matches("(^peny)[aiueo].*")) {
          kata = s.replaceAll("(^peny)", "s");
          if (cariKamus(kata)) {
            return kata;
          }
          kataBaru = hapusDerivationSuffixes(kata);
          return kataBaru;
        }
        //============ rule 31 ===========
        if (s.matches(".*(^pel)[aiueo].*")) {
          kata = s.replaceAll("(^pel)", "l");
          if (cariKamus(kata)) {
            return kata;
          }
          kataBaru = hapusDerivationSuffixes(kata);
          if (cariKamus(kataBaru)) {
            return kataBaru;
          }
          kata = s.replaceAll("(^pel)", "");
          if (cariKamus(kata)) {
            return kata;
          }
          kataBaru = hapusDerivationSuffixes(kata);
          return kataBaru;
        }
        //============ rule 32 ===========
        if (s.matches("(^pe)[^aiueorwylmn]er[aiueo].*")) {
          kata = s.replaceAll("(^pe)", "");
          if (cariKamus(kata)) {
            return kata;
          }
          kataBaru = hapusDerivationSuffixes(kata);
          return kataBaru;
        }
        //============ rule 33 ===========
        if (s.matches("(^pe)[^aiueorwylmn](?!er).*")) {
          kata = s.replaceAll("(^pe)", "s");
          if (cariKamus(kata)) {
            return kata;
          }
          kataBaru = hapusDerivationSuffixes(kata);
          return kataBaru;
        }
      }
    }
    return kataStemAsal;
  }

}
